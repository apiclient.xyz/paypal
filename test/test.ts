import { expect, tap } from '@pushrocks/tapbundle';
import * as paypal from '../ts/index';

import { Qenv } from '@pushrocks/qenv';
const testQenv = new Qenv('./', './.nogit/');
import * as smarttime from '@pushrocks/smarttime';

let testPayPalInstance: paypal.PayPalAccount;

tap.test('should create a valid paypal instance', async () => {
  testPayPalInstance = new paypal.PayPalAccount({
    clientId: testQenv.getEnvVarOnDemand('PAYPAL_CLIENT_ID'),
    clientSecret: testQenv.getEnvVarOnDemand('PAYPAL_CLIENT_SECRET'),
    accountOwner: 'sample corp'
  });
  expect(testPayPalInstance).to.be.instanceOf(paypal.PayPalAccount);
});

tap.test('should get an access token', async () => {
  const transactions = await testPayPalInstance.getTransactionsFromTo(
    smarttime.ExtendedDate.fromHyphedDate('2020-01-01').getTime(),
    smarttime.ExtendedDate.fromHyphedDate('2020-08-01').getTime()
  );
  console.log(transactions);
});

tap.start();
